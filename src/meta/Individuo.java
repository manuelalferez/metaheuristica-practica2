/**
 * @author Manuel Alférez Ruiz
 * @note Metaheurística Práctica 2
 */

package meta;

/**
 * @brief Clase que representa a un individuo
 */
public class Individuo {
    // genotipo: Representación de la solución del problema
    protected int[] genotipo;
    protected int tam_gen;
    int generacion;
    int fitness;
    // NUM: valor que indica cuantos individuos se han creado a lo largo de la historia
    // cruzado: indicador si el individuo se ha cruzado por algún operador
    protected static int NUM = 0;
    protected boolean cruzado;
    public int id;

    /**
     * @brief Constructor
     * @param _tam_gen Tamaño del genotipo
     */
    protected Individuo(int _tam_gen) {
        genotipo = new int[tam_gen = _tam_gen];
        generacion = Main.generaciones;
        fitness = -1;
        id = NUM;
        NUM++;
        cruzado = false;
    } // constructor parametrizado parcial

    /**
     * @brief Constructor de copia
     * @param copia El individuo a copiar
     * @post Copiar el individuo copia
     */
    protected Individuo(Individuo copia) {
        genotipo= new int[tam_gen=copia.tam_gen];
        for (int i = 0; i < tam_gen; i++) {
            genotipo[i] = copia.genotipo[i];
        }
        generacion = Main.generaciones+1;
        fitness = copia.fitness;
        id = NUM;
        NUM++;
        cruzado = copia.cruzado;
    } // constructor de copia

    /**
     * @brief Método de copia de individuo
     * @param copia El individuo a copiar
     * @post Realizar una copia exacta del individuo copia
     */
    protected void copiar_individuo(Individuo copia) {
        tam_gen = copia.tam_gen;
        for (int i = 0; i < tam_gen; i++) {
            genotipo[i] = copia.genotipo[i];
        }
        generacion = Main.generaciones + 1;
        fitness = copia.fitness;
        id = copia.id;
        cruzado = copia.cruzado;
    } // copiar_individuo() -> Complejidad = n (tam_gen)

    /**
     * @brief Calcula el fitness completo del individuo
     * @param fabrica Fabrica en la que aplicar el algoritmo
     */
    protected void calcular_fitness(Fabrica fabrica) {
        fitness = 0;
        Main.evaluaciones++;
        for (int i = 0; i < fabrica.num_unidades; i++)
            for (int j = 0; j < fabrica.num_unidades; j++)
                if (i != j)
                    fitness += fabrica.piezas[i][j] * fabrica.distancias[genotipo[i]][genotipo[j]];
    } // calcular_fitness() -> Complejidad en tiempo= n²

    /**
     * @brief Busca un valor y devuelve su posición
     * @param valor El valor a buscar
     * @return La posición donde se encuentra el valor o -1 si no lo ha encontrado
     */
    protected int busca_valor(int valor) {
        for (int i = 0; i < tam_gen; i++) {
            if (genotipo[i] == valor) return i;
        }
        return -1;
    } // busca_valor() -> Complejidad = n

    /**
     * @brief Calcula el fitness del individuo después de realizar un intercambio
     * @param fabrica Fabrica en la que aplicar el algoritmo
     * @param r Posición de intercambio
     * @param s Posición de intercambio
     * @note Se realiza el intercambio de la posición dentro del algoritmo
     */
    protected void calcular_fitness_parcial(Fabrica fabrica, int r, int s) {
        // Cálculo del fitness parcial con las unidades implicadas antes de intercambiarlas
        int fitness_parcialA = 0;
        for (int i = 0; i < tam_gen; i++) {
            if (i != r) {
                fitness_parcialA += fabrica.piezas[r][i] * fabrica.distancias[genotipo[r]][genotipo[i]];
                fitness_parcialA += fabrica.piezas[i][r] * fabrica.distancias[genotipo[i]][genotipo[r]];
            }
            if (i != s) {
                fitness_parcialA += fabrica.piezas[s][i] * fabrica.distancias[genotipo[s]][genotipo[i]];
                fitness_parcialA += fabrica.piezas[i][s] * fabrica.distancias[genotipo[i]][genotipo[s]];
            }
        }
        int c = genotipo[r];
        genotipo[r] = genotipo[s];
        genotipo[s] = c;

        // Cálculo del fitness parcial con las unidades implicadas después de intercambiarlas
        int fitness_parcialB = 0;
        for (int i = 0; i < tam_gen; i++) {
            if (i != r) {
                fitness_parcialB += fabrica.piezas[r][i] * fabrica.distancias[genotipo[r]][genotipo[i]];
                fitness_parcialB += fabrica.piezas[i][r] * fabrica.distancias[genotipo[i]][genotipo[r]];
            }
            if (i != s) {
                fitness_parcialB += fabrica.piezas[s][i] * fabrica.distancias[genotipo[s]][genotipo[i]];
                fitness_parcialB += fabrica.piezas[i][s] * fabrica.distancias[genotipo[i]][genotipo[s]];
            }
        }

        // Cálculo del coste de la población al realizar el intercambio r, s
        fitness = fitness - (fitness_parcialA - fitness_parcialB);
    } // calcular_fitness_parcial() -> Complejidad = n-1

    /**
     * @brief Señalizar que el individuo ha sido cruzado
     */
    protected void set_cruzado() {
        cruzado = true;
    } // set_cruzado()

    /**
     * @brief Informar si el individuo ha sido cruzado
     * @return Devuelve true si ha sido cruzado y false en caso contrario
     */
    protected boolean ha_cruzado() {
        return cruzado;
    } // ha_cruzado()

} // class Individuo
