Resolución de un problema mediante el enfoque de dos algoritmos y comprobación
de la eficiencia de los mismos:

    - Algoritmo genético estacionario
    - Algoritmo genético generacional
    
Las especificaciones solucitadas para el proyecto se encuentra en el siguiente
enlace (PDF):

https://mega.nz/#!wGAjEKxJ!yoXR1kf3F1PskCvul0uH-jXdmU9EzH39Zzy6WlZKppw

El informe con la explicación del proyecto, así como la evaluación de los datos
obtenidos durante las ejecuciones se adjuntan en el siguiente enlace (PDF):

https://mega.nz/#!kWYVDICB!7Mt_l-q7YMLUjwY7QxDtpjMCdYNRrhFN62xxyGemZsI